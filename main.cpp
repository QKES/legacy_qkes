/*
 * Quantum Key Exchange Simulator (QKES)
 * Authored by Michael B McGregor
 * Under the advisement of Dr. Michael Haney
 * University of Idaho
 * 20171203
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <random>
#include <vector>
#include "includes/cxxopts.hpp"

struct Signal {
    uint16_t axis; // 0 is up/down, 1 is for left/right
    uint16_t spin; // 0 is up/left, 1 is down/right
    int seq; // Signal sequence number, not sure if I'll need this yet.
    Signal(int axis, int spin, int seq) : axis(axis), spin(spin), seq(seq) {}
    Signal() : axis(), spin(), seq() {}
} __attribute__((packed));

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

void wait() {
    std::cout << "Press Enter to continue" << std::endl;
    std::cin.ignore( std::numeric_limits<std::streamsize>::max(), '\n' );
}

// This function converts the marshalled network data back into the Signal struct.
static Signal decode_signal (uint32_t *recv_data, size_t recv_len) {
    if (recv_len < sizeof(struct Signal)) {
        error("received too little");
    }
    struct Signal *recv_signal = (struct Signal *)recv_data;
    struct Signal s;
    s.axis = ntohs(recv_signal->axis);
    s.spin = ntohs(recv_signal->spin);
    s.seq = (int)ntohl(recv_signal->seq);
    return s;
}

int main(int argc, char* argv[]) {
    // Prep and process arguments
    cxxopts::Options options("QKES", "Quantum Key Exchange Simulator");
    int quantumPort = -1; // Port to be used for "secure channel" communications
    int publicPort = -1; // Port to be used for observation or "public channel"
    int numberOfSignals = 100; // Number of key element signals to send
    std::string serverIP = "127.0.0.1"; // Default IP address for Server
    std::string clientIP = "127.0.0.1"; // Default IP address for Client
    std::string observerIP = "127.0.0.1"; // Default IP address for Observer
    struct sockaddr_in serverINET;
    struct sockaddr_in clientINET;
    struct sockaddr_in observerINET;
    bool isClient = false;
    bool isServer = false;

    { // These are the command line options QKES supports
        options.add_options()
                ("h,help", "View the help menu (this menu)")
                ("m,mode", "Program operation mode", cxxopts::value<std::string>())
                ("n,number-of-signals", "Number of key element signals to send (Default: 100). Only used if --mode=alice",
                 cxxopts::value(numberOfSignals))
                ("q,quantum-port", "Private/Quantum channel port", cxxopts::value(quantumPort))
                ("p,public-port", "Public channel port", cxxopts::value(publicPort))
                ("serverIP", "Server's IP address (Default: 127.0.0.1)", cxxopts::value(serverIP))
                ("clientIP", "Client's IP Address (Default: 127.0.0.1)", cxxopts::value(clientIP))
                ("observerIP", "Observer's IP Address (Default: 127.0.0.1)", cxxopts::value(observerIP));
        options.parse(argc, argv);

        // Check supplied parameters
        std::string programMode = options["mode"].as<std::string>();
        if (options.count("help")) {
            std::cout << options.help() << std::endl;
            return 0;
        }
        if (programMode != "alice" && programMode != "bob") {
            std::cout
                    << "Fatal error with provided arguments. Program mode (-m) must be one of 'alice' or 'bob'."
                    << std::endl;
            return 1;
        }
        if (programMode == "alice") isClient = true;
        if (programMode == "bob") isServer = true;
        if (quantumPort != -1 && (quantumPort <= 1024 || quantumPort > 65536)) {
            std::cout << "Fatal error with provided arguments. Quantum port not a valid port (1025..65536)"
                      << std::endl;
            return 1;
        }
        if (quantumPort != -1 && (quantumPort <= 1024 || publicPort > 65536)) {
            std::cout << "Fatal error with provided arguments. Public port not a valid port (1025..65536)" << std::endl;
            return 1;
        }
        // Convert supplied IP addresses to
        inet_pton(AF_INET, serverIP.c_str(), &(serverINET.sin_addr));
        inet_pton(AF_INET, clientIP.c_str(), &(clientINET.sin_addr));
        inet_pton(AF_INET, observerIP.c_str(), &(observerINET.sin_addr));
    }

    if (isServer) {
        // Server segment of code
        // Set up random number system
        std::random_device rd;
        std::mt19937 e2(rd());
        std::uniform_int_distribution<int> dist(0,1);

//    // Variable declarations
        int sockfd, newsockfd, n;
        socklen_t clilen;
        struct Signal source;
        int countKeep = 0;
        int countTrash = 0;
        std::vector<Signal> receivedSignals;

//    // Do some work to set up socket
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd < 0)
            error("ERROR opening socket");
        serverINET.sin_family = AF_INET;
        serverINET.sin_addr.s_addr = INADDR_ANY;
        serverINET.sin_port = htons(static_cast<uint16_t>(quantumPort));
        if (bind(sockfd, (struct sockaddr *) &serverINET, sizeof(serverINET)) < 0)
            error("ERROR on binding");
        listen(sockfd, 50);
        clilen = sizeof(clientINET);
        newsockfd = accept(sockfd,
                           (struct sockaddr *) &clientINET,
                           &clilen);
        if (newsockfd < 0)
            error("ERROR on accept");

        //Phase 1: I am receiving signals from Alice
        bool receive = true;
        numberOfSignals = 0;
        while (receive) {
            n = read(newsockfd, (void *)&source, sizeof(Signal));
            if (n < 0)
                error("ERROR reading from socket");
            uint32_t *recv_data = (uint32_t *)&source;
            size_t recv_len = sizeof(source);

            // Store received Signal
            Signal received = decode_signal(recv_data, recv_len);
            if (received.seq ==-1) {
                receive = false;
                break;
            }
            else numberOfSignals++;
            // Perform random measurement on Signal
            int guess = dist(e2);
            if (guess == received.axis) {
                // If measurement succeeded, record spin.
                receivedSignals.emplace_back((int)received.axis,(int)received.spin,(int)received.seq);
//                countKeep++;
            }
            else { // If measurement was no good, record how we measured and what seq this was
                receivedSignals.emplace_back(guess,2,(int)received.seq);
//                countTrash++;
            }
        }
        close(newsockfd);
        close(sockfd);

        // Report on our received signals
        std::cout << "Phase 1 complete, I have received " << receivedSignals.size() << " signals from Alice." << std::endl;

        //Phase 2: Alice will be sending me which axis each particle was oriented on publicly
        // Prepare to listen for signals on public port
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd < 0)
            error("ERROR opening socket");
        serverINET.sin_port = htons(static_cast<uint16_t>(publicPort));
        if (bind(sockfd, (struct sockaddr *) &serverINET, sizeof(serverINET)) < 0)
            error("ERROR on binding");
        listen(sockfd, 50);
        newsockfd = accept(sockfd,
                           (struct sockaddr *) &clientINET,
                           &clilen);
        if (newsockfd < 0)
            error("ERROR on accept");

        //Receive signals
        std::vector<Signal> aliceAxes;
        for (int i = 0; i < numberOfSignals; i++) {
            n = read(newsockfd, (void *)&source, sizeof(Signal));
            if (n < 0)
                error("ERROR reading from socket");
            uint32_t *recv_data = (uint32_t *)&source;
            size_t recv_len = sizeof(source);

            // Store received Signal
            Signal received = decode_signal(recv_data, recv_len);
            aliceAxes.emplace_back((int)received.axis,2,(int)received.seq);
        }
        close(newsockfd);
        close(sockfd);

        //Report
        std::cout << "Phase 2 complete. I have received " << aliceAxes.size() << " axes from Alice for comparison." << std::endl;
        wait();

        //Phase 3: I need to publicly announce which filter I used for each particle received
        //Prepare socket for send
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd < 0)
            error("ERROR opening socket");
        clientINET.sin_family = AF_INET;
        clientINET.sin_port = htons(publicPort);
        if (connect(sockfd,(sockaddr *) &clientINET,sizeof(clientINET)) < 0)
            error("ERROR connecting");

        //Send signals
        for (Signal received : receivedSignals) {
            Signal signal;
            signal.axis = htons(received.axis);
            signal.spin = htons(2);
            signal.seq = htonl(received.seq);
            n = write(sockfd,(void*)&signal,sizeof(signal));
            if (n < 0)
                error("ERROR writing to socket");
        }
        close(sockfd);

        //Report
        std::cout << "Phase 3 complete. I have publicly sent the axes of each signal to Alice for her review." << std::endl;

        //Phase 4: Throw out every particle that didn't match
        std::vector<Signal> keyCandidate;
        for (int seq = 0; seq < numberOfSignals; seq++) {
            if (receivedSignals[seq].axis == aliceAxes[seq].axis)
                keyCandidate.emplace_back((int)receivedSignals[seq].axis,(int)receivedSignals[seq].spin,(int)receivedSignals[seq].seq);
        }
        std::cout << "Phase 4 complete. Agreed upon key (" << keyCandidate.size() << " bits) is as follows:" << std::endl;
        for (Signal signal : keyCandidate) {
            std::cout << signal.spin;
        }
        std::cout << std::endl;


        //Phase 5: Choose a small subset and compare publicly to determine if they were modified

        return 0;
    }

    if (isClient) {
        // Initialize random number generator and socket
        int sockfd, newsockfd, n;
        socklen_t srvlen;
        std::random_device rd;
        std::mt19937 e2(rd());
        std::uniform_int_distribution<int> dist(0,1);
        std::vector<Signal> sentSignals;
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd < 0)
            error("ERROR opening socket");
        serverINET.sin_family = AF_INET;
        serverINET.sin_port = htons(quantumPort);
        if (connect(sockfd,(sockaddr *) &serverINET,sizeof(serverINET)) < 0)
            error("ERROR connecting");

        //Phase 1: I will generate and transmit random signals to Bob
        for (int seq = 0; seq < numberOfSignals; seq++) {
            int axis = dist(e2);
            int spin = dist(e2);
            sentSignals.emplace_back(axis,spin,seq);
            Signal source;
            source.axis = htons(axis);
            source.spin = htons(spin);
            source.seq = htonl(seq);
            n = write(sockfd,(void*)&source,sizeof(source));
            if (n < 0)
                error("ERROR writing to socket");
        }
        { //Send one last signal to "complete" the transmission
            Signal source;
            source.axis = htons(0);
            source.spin = htons(0);
            source.seq = htonl((uint32_t)-1);
            n = write(sockfd,(void*)&source,sizeof(source));
            if (n < 0)
                error("ERROR writing to socket");
        }


        close(sockfd);
        std::cout << "Phase 1 complete, I have sent " << sentSignals.size() << " randomly generated signals to Bob." << std::endl;
        wait();

        //Phase 2: I now need to publicly announce to Bob for each particle if it was oriented on the x-axis or the y-axis.
        //Create socket on publicPort
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd < 0)
            error("ERROR opening socket");
        serverINET.sin_port = htons(publicPort);
        if (connect(sockfd,(sockaddr *) &serverINET, sizeof(serverINET)) < 0)
            error("ERROR connecting");

        //Send each axis publicly
        for (Signal signal : sentSignals ) {
            Signal packet;
            packet.axis = htons(signal.axis);
            packet.spin = htons(2);
            packet.seq = htonl(signal.seq);
            n = write(sockfd, (void *) &packet, sizeof(packet));
            if (n < 0)
                error("ERROR writing to socket");
        }
        close(sockfd);
        std::cout << "Phase 2 complete. I have publicly sent the axes of each signal to Bob for his review." << std::endl;
        wait();

        //Phase 3: Bob is going to publicly announce which measures he used. For this simulation, he will send that to me over a "public" socket
        // Prepare to listen for signals on public port
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd < 0)
            error("ERROR opening socket");
//        int reuse = 1;
//        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (const char*)&reuse, sizeof(int)) < 0)
//            error("setsockopt(SO_REUSEADDR) failed");
//#ifdef SO_REUSEPORT
//        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEPORT, (const char*)&reuse, sizeof(reuse)) < 0)
//            perror("setsockopt(SO_REUSEPORT) failed");
//#endif
        clientINET.sin_family = AF_INET;
        clientINET.sin_addr.s_addr = INADDR_ANY;
        clientINET.sin_port = htons(static_cast<uint16_t>(publicPort));
        srvlen = sizeof(serverINET);
        if (bind(sockfd, (struct sockaddr *) &clientINET, sizeof(clientINET)) < 0)
            error("ERROR on binding");
        listen(sockfd, 50);
        newsockfd = accept(sockfd,
                           (struct sockaddr *) &serverINET,
                           &srvlen);
        if (newsockfd < 0)
            error("ERROR on accept");

        // Receive signals
        std::vector<Signal> bobAxes;
        Signal source;
        for (int i = 0; i < numberOfSignals; i++) {
            n = read(newsockfd, (void *)&source, sizeof(Signal));
            if (n < 0)
                error("ERROR reading from socket");
            uint32_t *recv_data = (uint32_t *)&source;
            size_t recv_len = sizeof(source);

            // Store received Signal
            Signal received = decode_signal(recv_data, recv_len);
            bobAxes.emplace_back((int)received.axis,2,(int)received.seq);
        }
        close(newsockfd);
        close(sockfd);

        //Report
        std::cout << "Phase 3 complete. I have received " << bobAxes.size() << " axes from Bob for comparison." << std::endl;

        //Receive signals

        //Phase 4: Throw out every particle that didn't match
        std::vector<Signal> keyCandidate;
        for (int seq = 0; seq < numberOfSignals; seq++) {
            if (sentSignals[seq].axis == bobAxes[seq].axis)
                keyCandidate.emplace_back((int)sentSignals[seq].axis,(int)sentSignals[seq].spin,(int)sentSignals[seq].seq);
        }
        std::cout << "Phase 4 complete. Agreed upon key (" << keyCandidate.size() << " bits) is as follows:" << std::endl;
        for (Signal signal : keyCandidate) {
            std::cout << signal.spin;
        }
        std::cout << std::endl;

        //Phase 5: Choose a small subset and compare publicly to determine if they were modified
        return 0;
    }

}
